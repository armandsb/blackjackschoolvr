﻿using UnityEngine;
using System.Collections;

public class StrategyTutorial : BaseTutorial {

	// Use this for initialization
	 void Start () {
		base.startAudio ();
		GameObject.Find("nextTutorialButtonText").gameObject.GetComponent<TextMesh>().text = "Test";
		StartCoroutine (showCardCombinations());
	}

	override protected AudioClip getTutorialClip() {
		return Resources.Load("Sounds/Blackjack_tutorial_sounds/StrategyTutorialSound", typeof(AudioClip)) as AudioClip;
	}

	IEnumerator showCardCombinations() {
		yield return new WaitForSeconds(20);
		GameObject.Find ("Tutorial GO").transform.Find ("StrategyTutorialGO").transform.Find ("StiffHandPlayer").gameObject.SetActive (true);
		yield return new WaitForSeconds(7);
		GameObject.Find ("Tutorial GO").transform.Find ("StrategyTutorialGO").transform.Find ("StiffHandDealer").gameObject.SetActive (true);
		yield return new WaitForSeconds(8);
		hideAll ();
		GameObject.Find ("Tutorial GO").transform.Find ("StrategyTutorialGO").transform.Find ("PatHandPlayer").gameObject.SetActive (true);
		yield return new WaitForSeconds(6);
		GameObject.Find ("Tutorial GO").transform.Find ("StrategyTutorialGO").transform.Find ("PatHandDealer").gameObject.SetActive (true);
		yield return new WaitForSeconds(13);
		StartCoroutine (bothStiffStrategy());
	}

	IEnumerator bothStiffStrategy() {
		hideAll ();
		GameObject.Find ("Tutorial GO").transform.Find ("StrategyTutorialGO").transform.Find ("StiffHandPlayer").gameObject.SetActive (true);
		GameObject.Find ("Tutorial GO").transform.Find ("StrategyTutorialGO").transform.Find ("StiffHandDealer").gameObject.SetActive (true);
		yield return new WaitForSeconds(5);
		GameObject.Find ("Tutorial GO").transform.Find ("StrategyTutorialGO").transform.Find ("strategyStand").gameObject.SetActive (true);
		yield return new WaitForSeconds(2);
		StartCoroutine (stiffAndPatStrategy());
	}

	IEnumerator stiffAndPatStrategy() {
		hideAll ();
		GameObject.Find ("Tutorial GO").transform.Find ("StrategyTutorialGO").transform.Find ("StiffHandPlayer").gameObject.SetActive (true);
		GameObject.Find ("Tutorial GO").transform.Find ("StrategyTutorialGO").transform.Find ("PatHandDealer").gameObject.SetActive (true);
		yield return new WaitForSeconds(6);
		GameObject.Find ("Tutorial GO").transform.Find ("StrategyTutorialGO").transform.Find ("strategyHit").gameObject.SetActive (true);
		yield return new WaitForSeconds(3);
		StartCoroutine (patStrategy());
	}

	IEnumerator patStrategy() {
		hideAll ();
		GameObject.Find ("Tutorial GO").transform.Find ("StrategyTutorialGO").transform.Find ("PatHandPlayer").gameObject.SetActive (true);
		yield return new WaitForSeconds(5);
		GameObject.Find ("Tutorial GO").transform.Find ("StrategyTutorialGO").transform.Find ("strategyStand").gameObject.SetActive (true);
	}

	private void hideAll() 
	{
		GameObject.Find ("Tutorial GO").transform.Find ("StrategyTutorialGO").transform.Find ("StiffHandPlayer").gameObject.SetActive (false);
		GameObject.Find ("Tutorial GO").transform.Find ("StrategyTutorialGO").transform.Find ("StiffHandDealer").gameObject.SetActive (false);
		GameObject.Find ("Tutorial GO").transform.Find ("StrategyTutorialGO").transform.Find ("PatHandPlayer").gameObject.SetActive (false);
		GameObject.Find ("Tutorial GO").transform.Find ("StrategyTutorialGO").transform.Find ("PatHandDealer").gameObject.SetActive (false);
		GameObject.Find ("Tutorial GO").transform.Find ("StrategyTutorialGO").transform.Find ("strategyStand").gameObject.SetActive (false);
		GameObject.Find ("Tutorial GO").transform.Find ("StrategyTutorialGO").transform.Find ("strategyHit").gameObject.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		if (!audio.isPlaying) {
			PlayerManager.Instance.blackJackManager.GetComponent<TutorialManager> ().enableTutorialButtons (true);
		} 
	}

	void OnDestroy() {
		GameObject nextButton = GameObject.Find ("nextTutorialButtonText");
		if (nextButton != null) {
			nextButton.gameObject.GetComponent<TextMesh>().text = "Next";
		}
		hideAll ();
	}
}
