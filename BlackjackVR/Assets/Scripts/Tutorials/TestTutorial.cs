﻿using UnityEngine;
using System.Collections;
using Blackjack;

public class TestTutorial : BaseTutorial {

	private int numberOfTest = 0;

	// Use this for initialization
	void Start () {
		GameObject.Find("Buttons").transform.Find ("GameButtons").gameObject.SetActive (true);
		GameObject.Find("Tutorial GO").transform.Find("Action buttons").gameObject.SetActive (false);
		startTutorial ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
		
	public void startTutorial() {
		numberOfTest++;
		GameSelectionManager manager = PlayerManager.Instance.blackJackManager.transform.GetComponent<GameSelectionManager> ();
		if (manager.currentGame != null) {
			Destroy (manager.currentGame);
			manager.currentGame = null;
		}

		if (numberOfTest > 9) {
			PlayerManager.Instance.blackJackManager.transform.GetComponent<GameSelectionManager> ().enterTable ();
		} else {
			manager.currentGame = manager.gameObject.AddComponent<TestBlackjackGame> ();
			manager.currentGame.newGame(getGame(),0);
		}
	}

	private Game getGame() {
		switch (numberOfTest) {
		case 1:
			return new Game (PlayerManager.Instance.deck.crateScenario1());
		case 2:
			return new Game (PlayerManager.Instance.deck.crateScenario2());
		case 3:
			return new Game (PlayerManager.Instance.deck.crateScenario3());
		case 4:
			return new Game (PlayerManager.Instance.deck.crateScenario4());
		case 5:
			return new Game (PlayerManager.Instance.deck.crateScenario5());
		case 6:
			return new Game (PlayerManager.Instance.deck.crateScenario6());
		case 7:
			return new Game (PlayerManager.Instance.deck.crateScenario7());
		case 8:
			return new Game (PlayerManager.Instance.deck.crateScenario8());
		default:
			return new Game (PlayerManager.Instance.deck.crateScenario9());
		}
	}
		
	override protected AudioClip getTutorialClip(){
		return null;
	}
}
