﻿using UnityEngine;
using System.Collections;

public class TutorialManager : MonoBehaviour, TimingScript {

	public GameObject tutorialButtons;
	int currentTutorial = 0;
	public BaseTutorial currentTutorialGO;

	void Start () {
		tutorialButtons = GameObject.Find("Tutorial GO").transform.Find("Action buttons").gameObject;
	}

	public void HandleTimedInput() {

		if (gameObject.GetComponent<Renderer>().material.color.a == 0.5) {
			return;
		}

		GameObject prevTutorialButton = tutorialButtons.transform.Find("prevTutorialButton").gameObject;
		GameObject nextTutorialButton = tutorialButtons.transform.Find("nextTutorialButton" ).gameObject;

		if (prevTutorialButton != null && prevTutorialButton.gameObject == transform.gameObject) {
			PlayerManager.Instance.blackJackManager.GetComponent<TutorialManager>().prevTutorial ();
		}else if (nextTutorialButton != null && nextTutorialButton.gameObject == transform.gameObject) {
			PlayerManager.Instance.blackJackManager.GetComponent<TutorialManager>().nextTutorial ();
		}
	}

	// Update is called once per frame
	void Update () {
	
	}

	// Tutorial

	private void nextTutorial() {
		if (currentTutorial >= 6) {
			return;
		}

		currentTutorial++;
		showTutorial ();
		updateTutorialStateButtons ();
	}

	private void prevTutorial() {
		if (currentTutorial <= 0) {
			return;
		}
		currentTutorial--;
		showTutorial ();
		updateTutorialStateButtons ();
	}

	private void updateTutorialStateButtons() {
		if (currentTutorial == 0) {
			tutorialButtons.transform.Find ("prevTutorialButton").gameObject.SetActive (false);
		} else {
			tutorialButtons.transform.Find ("prevTutorialButton").gameObject.SetActive (true);
		}

		if (currentTutorial >= 6) {
			tutorialButtons.transform.Find ("nextTutorialButton").gameObject.SetActive (false);
		} else {
			tutorialButtons.transform.Find ("nextTutorialButton").gameObject.SetActive (true);
		}
	}

	private void showTutorial() {
		if (currentTutorialGO != null) {
			Destroy (currentTutorialGO);
		}

		AudioSource audio = PlayerManager.Instance.mainDealer.GetComponent<AudioSource> () as AudioSource;
		audio.Stop ();

		switch (currentTutorial) {
		case 0:
			currentTutorialGO = gameObject.AddComponent<IntroTutorial> ();
			break;
		case 1:
			currentTutorialGO = gameObject.AddComponent<RulesTutorial> ();
			break;
		case 2:
			currentTutorialGO = gameObject.AddComponent<PointsTutorial> ();
			break;
		case 3:
			currentTutorialGO = gameObject.AddComponent<OptionsTutorial> ();
			break;
		case 4:
			currentTutorialGO = gameObject.AddComponent<StrategyTutorial> ();
			break;
		case 5:
			currentTutorialGO = gameObject.AddComponent<TestTutorial> ();
			break;
		default:
			break;
		}

		enableTutorialButtons (false);
	}

	public void enableTutorialButtons(bool enabled) {
		if (enabled == true) {
			tutorialButtons.transform.Find ("prevTutorialButton").gameObject.GetComponent<Renderer>().material.color = new Color (1.0f, 1.0f, 1.0f, 1.0f);
			tutorialButtons.transform.Find ("nextTutorialButton").gameObject.GetComponent<Renderer>().material.color = new Color (1.0f, 1.0f, 1.0f, 1.0f);
		} else {
			tutorialButtons.transform.Find ("prevTutorialButton").gameObject.GetComponent<Renderer>().material.color = new Color(1.0f, 1.0f, 1.0f, 0.5f);
			tutorialButtons.transform.Find ("nextTutorialButton").gameObject.GetComponent<Renderer>().material.color = new Color(1.0f, 1.0f, 1.0f, 0.5f);
		}
	}

	public void startTutorial() {
		currentTutorial = 0;
		updateTutorialStateButtons ();
		showTutorial ();
	}

	public void endTutorial() {
		if (currentTutorialGO != null) {
			Destroy (currentTutorialGO);
		}

	}
}
