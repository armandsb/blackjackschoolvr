﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public interface TimingScript : IEventSystemHandler {
	void HandleTimedInput();
}
