﻿using UnityEngine;
using System.Collections;
using Blackjack;

public class GameSelectionManager : MonoBehaviour, TimingScript {

	GameObject allButtons;
    public BlackjackGame currentGame;

	// Use this for initialization
	void Start () {
		allButtons = GameObject.Find("Buttons");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void HandleTimedInput() {
		GameObject endButton = allButtons.transform.Find("endButton").gameObject;
		GameObject gameButton = allButtons.transform.Find( "StartButtons" ).Find("gameButton").gameObject;
		GameObject tutorialButton = allButtons.transform.Find( "StartButtons" ).Find("tutorialButton").gameObject;

		if (gameButton != null && gameButton.gameObject == transform.gameObject) {
			PlayerManager.Instance.blackJackManager.GetComponent<GameSelectionManager>().startGame ();
		}else if (tutorialButton != null && tutorialButton.gameObject == transform.gameObject) {
			PlayerManager.Instance.blackJackManager.GetComponent<GameSelectionManager>().startTutorial ();
		} else if (endButton != null && endButton.gameObject == transform.gameObject) {
			PlayerManager.Instance.blackJackManager.GetComponent<GameSelectionManager>().dismissFromTable ();
		} else {
			var heading = transform.position - PlayerManager.Instance.player.transform.position;
			var distance = Vector3.Dot(heading, PlayerManager.Instance.player.transform.forward);
			Debug.Log (distance.ToString());
			if (distance < 2) {
				PlayerManager.Instance.blackJackManager.GetComponent<GameSelectionManager>().enterTable ();
			}
		}
	}

	//Tutorial

	private void startTutorial() {
	   PlayerManager.Instance.state = PlayerState.tutorial;
	   allButtons.transform.Find ("StartButtons").gameObject.SetActive (false);
	   GameObject.Find ("Tutorial GO").transform.Find ("Action buttons").gameObject.SetActive (true);
	   PlayerManager.Instance.blackJackManager.GetComponent<TutorialManager>().startTutorial();
	}

	//Game

	private void startGame() {
		enterBidsState ();
	}

	public void enterBidsState() {
		PlayerManager.Instance.state = PlayerState.bids;
		allButtons.transform.Find ("StartButtons").gameObject.SetActive (false);
		allButtons.transform.Find ("RateButtons").gameObject.SetActive (true);
		allButtons.transform.Find ("DealingButtons").gameObject.SetActive (true);
		allButtons.transform.Find ("GameButtons").gameObject.SetActive (false);
		GameObject.Find ("BiddingUI").transform.Find ("BiddingUIBox").gameObject.SetActive (true);
		GetComponent<Bidding> ().startBidding ();
	}

	public void enterGameState(int bid) {
		PlayerManager.Instance.state = PlayerState.game;
		allButtons.transform.Find ("GameButtons").gameObject.SetActive (true);
		allButtons.transform.Find ("RateButtons").gameObject.SetActive (false);
		allButtons.transform.Find ("DealingButtons").gameObject.SetActive (false);
		GameObject.Find ("BiddingUI").transform.Find ("BiddingUIBox").gameObject.SetActive (false);

		if (currentGame != null) {
			Destroy (currentGame);
		}

		currentGame = gameObject.AddComponent<BlackjackGame> ();
		currentGame.newGame(new Game(PlayerManager.Instance.deck.createDeck()),bid);
	}


	//Start/End actions

	public void enterTable() {
		PlayerManager.Instance.state = PlayerState.table;
		GameObject startButtons = allButtons.transform.Find( "StartButtons" ).gameObject;
		allButtons.transform.Find("endButton").gameObject.SetActive (true);
		allButtons.transform.Find ("GameButtons").gameObject.SetActive (false);

		startButtons.SetActive (true);
		PlayerManager.Instance.player.transform.position = new Vector3 (-5.07f, 2.173781f, -0.45f);
		PlayerManager.Instance.player.GetComponent <Autowalk>().enabled = false;
		SetAllCollidersStatus (false);
	}

	private void dismissFromTable() {
		PlayerManager.Instance.state = PlayerState.lobby;
		GameObject.Find ("BiddingUI").transform.Find ("BiddingUIBox").gameObject.SetActive (false);
		allButtons.transform.Find ("StartButtons").gameObject.SetActive (false);
		allButtons.transform.Find ("RateButtons").gameObject.SetActive (false);
		allButtons.transform.Find ("DealingButtons").gameObject.SetActive (false);
		allButtons.transform.Find ("GameButtons").gameObject.SetActive (false);
		allButtons.transform.Find ("endButton").gameObject.SetActive (false);
		GameObject.Find ("Tutorial GO").transform.Find ("Action buttons").gameObject.SetActive (false);
		PlayerManager.Instance.player.transform.position = new Vector3 (-5.07f, 2.27f, -2.35f);
		SetAllCollidersStatus (true);
		PlayerManager.Instance.player.GetComponent <Autowalk>().enabled = true;
		PlayerManager.Instance.blackJackManager.GetComponent<TutorialManager>().endTutorial();
		AudioSource audio = PlayerManager.Instance.mainDealer.GetComponent<AudioSource> () as AudioSource;
		audio.Stop ();

		if (currentGame != null) {
			currentGame.clearObjects ();
			Destroy (currentGame);
		}
	}

	private void SetAllCollidersStatus (bool active) {
		GameObject chair = GameObject.Find ("Player chair");
		foreach(Collider c in chair.GetComponents<Collider> ()) {
			c.enabled = active;
		}
	}
		
		
}
